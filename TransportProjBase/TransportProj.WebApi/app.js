﻿(function() {
    
    var jsonData = $.ajax({
        url: '../api/CarRoutes',
        dataType: 'json',
    }).done(function (results) {

        var ctx = document.getElementById("carPathChart");

        var myChart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    label: "Y",
                    fill: false,
                    backgroundColor: "rgba(75,192,192,0.4)",
                    borderColor: "rgba(75,192,192,1)",
                    borderCapStyle: 'butt',
                    borderDash: [],
                    borderDashOffset: 0.0,
                    borderJoinStyle: 'miter',
                    pointBorderColor: "rgba(75,192,192,1)",
                    pointBackgroundColor: "#000",
                    pointBorderWidth: 10,
                    pointHoverRadius: 5,
                    pointHoverBackgroundColor: "rgba(75,192,192,1)",
                    pointHoverBorderColor: "rgba(220,220,220,1)",
                    pointHoverBorderWidth: 2,
                    pointRadius: 20,
                    pointHitRadius: 20,
                    data: results
                }]
            },
            options: {
                title: {
                    display: true,
                    text: "Car Route Path"
                },
                scales: {
                    xAxes: [{
                        type: 'linear',
                        position: 'bottom'
                    }]
                }
            }
        });
    })

}());
