﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Web.Http;

namespace TransportProj.WebApi.Controllers
{
    public class CarRoutesController : ApiController
    {
        // GET: api/CarRoutes
        public HttpResponseMessage Get()
        {
            // -- get car routes data 
            string appDatapath = AppDomain.CurrentDomain.GetData("DataDirectory").ToString();
            var filePath = $"{appDatapath}\\carroute.json";

            var stream = new FileStream(filePath, FileMode.Open);

            var result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new StreamContent(stream);
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");

            return result;
        }
    }
}
