﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj.Utilities
{
    public class CarPathUtil
    {
        private List<CarPath> CarPathList;

        public CarPathUtil()
        {
            CarPathList = new List<CarPath>();
        }

        public void BuildCarPath(Car car)
        {
            var newCarPath = new CarPath
            {
                x = car.XPos,
                y = car.YPos
            };

            CarPathList.Add(newCarPath);
        }

        public void LogCarPathCoordinates()
        {
            var json = JsonConvert.SerializeObject(CarPathList, Formatting.Indented);

            // -- add json data to WebApi project APP_DATA folder
            string path = $"{Directory.GetParent(Directory.GetCurrentDirectory()).Parent.FullName}";
            string newFilePath = Path.GetFullPath(Path.Combine(path, @"..\transportProj.WebApi\App_Data\carroute.json"));

            File.WriteAllText(newFilePath, json);
        }
    }
}
