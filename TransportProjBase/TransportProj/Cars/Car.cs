﻿using System;

namespace TransportProj
{
    public abstract class Car
    {
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public Passenger Passenger { get; private set; }
        public City City { get; private set; }
        public bool IsRaceCar { get; protected set; }

        public Car(int xPos, int yPos, City city, Passenger passenger)
        {
            XPos = xPos;
            YPos = yPos;
            City = city;
            Passenger = passenger;
            IsRaceCar = false;
        }

        protected virtual void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Car moved to x - {0} y - {1}", XPos, YPos));
        }

        public void PickupPassenger(Passenger passenger)
        {
            Passenger = passenger;
        }

        public virtual void MoveUp()
        {
            throw new NotImplementedException();
        }

        public virtual void MoveDown()
        {
            throw new NotImplementedException();
        }

        public virtual void MoveRight()
        {
            throw new NotImplementedException();
        }

        public virtual void MoveLeft()
        {
            throw new NotImplementedException();
        }

        public virtual void MoveUp(int startYPos, int endYPos)
        {
            throw new NotImplementedException();
        }

        public virtual void MoveDown(int startYPos, int endYPos)
        {
            throw new NotImplementedException();
        }

        public virtual void MoveRight(int startYPos, int endYPos)
        {
            throw new NotImplementedException();
        }

        public virtual void MoveLeft(int startYPos, int endYPos)
        {
            throw new NotImplementedException();
        }
    }
}
