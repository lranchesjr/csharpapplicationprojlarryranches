﻿using System;

namespace TransportProj
{
    public class RaceCar : Car
    {
        public RaceCar(int xPos, int yPos, City city, Passenger passenger) : base(xPos, yPos, city, passenger)
        {
            IsRaceCar = true;
        }

        // race car y pos move up
        public override void MoveUp(int startYPos, int endYPos)
        {
            if (YPos < City.YMax)
            {

                if ((endYPos - startYPos) > 1)
                {
                    YPos += 2;
                    Console.WriteLine("* Race car moved 2 spaces UP *\n");
                }
                else
                {
                    YPos++;
                }

                WritePositionToConsole();
            }
        }

        // race car move down
        public override void MoveDown(int startYPos, int endYPos)
        {
            if (YPos > 0)
            {

                if ((startYPos - endYPos) > 1)
                {
                    YPos -= 2;
                    Console.WriteLine("* Race car moved 2 spaces DOWN *\n");
                }
                else
                {
                    YPos--;
                }

                WritePositionToConsole();
            }
        }

        // race car move right
        public override void MoveRight(int startXPos, int endXPos)
        {
            if (XPos < City.XMax)
            {
                if ((endXPos - startXPos) > 1)
                {
                    XPos += 2;
                    Console.WriteLine("* Race car moved 2 spaces RIGHT *\n");
                }
                else
                {
                    XPos++;
                }

                WritePositionToConsole();
            }
        }

        // race car move left
        public override void MoveLeft(int startXPos, int endXPos)
        {
            if (XPos > 0)
            {
                if ((startXPos - endXPos) > 1)
                {
                    XPos -= 2;
                    Console.WriteLine("*Race car moved 2 spaces LEFT *\n");
                }
                else
                {
                    XPos--;
                }

                WritePositionToConsole();
            }
        }

        protected override void WritePositionToConsole()
        {
            Console.WriteLine(String.Format("Race Car moved to x - {0} y - {1}\n", XPos, YPos));
        }
    }
}
