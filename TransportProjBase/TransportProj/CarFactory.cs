﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class CarFactory
    {
        public Car CreateCar(CarType carType, int xPos, int yPos, City city)
        {
            switch(carType)
            {
                case CarType.Sedan:
                    return new Sedan(xPos, yPos, city, null);
                case CarType.Truck:
                    return new Truck(xPos, yPos, city, null);
                case CarType.RaceCar:
                    return new RaceCar(xPos, yPos, city, null);
                default:
                    return new Sedan(xPos, yPos, city, null);
            }
        }

        public enum CarType
        {
            Sedan,
            RaceCar,
            Truck
        }
    }
}
