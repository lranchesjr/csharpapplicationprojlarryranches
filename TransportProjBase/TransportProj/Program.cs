﻿using System;
using System.Collections.Generic;
using System.IO;
using TransportProj.Utilities;
using static TransportProj.CarFactory;

namespace TransportProj
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            int CityLength = 10;
            int CityWidth = 10;

            City MyCity = new City(CityLength, CityWidth);
            
            // -- create an instance of a Car Factory
            CarFactory carFactory = new CarFactory();

            // -- use Car Factory to create cars
            //Car car = carFactory.CreateCar(CarType.RaceCar, rand.Next(CityLength - 1), rand.Next(CityWidth - 1), MyCity);
            //Car car = carFactory.CreateCar(CarType.Sedan, rand.Next(CityLength - 1), rand.Next(CityWidth - 1), MyCity);
            Car car = carFactory.CreateCar(CarType.Truck, rand.Next(CityLength - 1), rand.Next(CityWidth - 1), MyCity);

            Passenger passenger = MyCity.AddPassengerToCity(rand.Next(CityLength - 1), rand.Next(CityWidth - 1), rand.Next(CityLength - 1), rand.Next(CityWidth - 1));

            Console.WriteLine("--------------------------------------------");
            Console.WriteLine($"CAR START: ({car.XPos}, {car.YPos})");
            Console.WriteLine($"PASSENGER PICKUP: ({passenger.StartingXPos}, {passenger.StartingYPos})");
            Console.WriteLine($"PASSENGER DESTINATION: ({passenger.DestinationXPos}, {passenger.DestinationYPos})");
            Console.WriteLine("--------------------------------------------\n\n");

            // -- create car path util
            var carPathUtil = new CarPathUtil();

            // -- add car starting coordinates to list
            carPathUtil.BuildCarPath(car);

            while (!passenger.IsAtDestination())
            {
                Tick(car, passenger);

                // -- add new car path to list
                carPathUtil.BuildCarPath(car);
            }

            // -- log car route data
            carPathUtil.LogCarPathCoordinates();

            Console.WriteLine("---- PASSENGER DROPPED OFF -----");
            Console.ReadLine();
            return;
        }
        
        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        private static void Tick(Car car, Passenger passenger)
        {
            if (car.Passenger == null)
            {
                TransportHelper(TransportType.PickUp, car, passenger);
            }
            else
            {
                TransportHelper(TransportType.DropOff, car, passenger);
            }
        }

        private static void TransportHelper(TransportType transportType, Car car, Passenger passenger)
        {
            // -- set helper variables
            int startXPos = 0, startYPos = 0, endXPos = 0, endYPos = 0;

            // -- set variables based on transport type
            if (transportType == TransportType.PickUp)
            {
                startXPos = car.XPos;
                endXPos = passenger.GetCurrentXPos();
                startYPos = car.YPos;
                endYPos = passenger.GetCurrentYPos();
            }

            if (transportType == TransportType.DropOff)
            {
                startXPos = car.Passenger.GetCurrentXPos();
                endXPos = car.Passenger.DestinationXPos;
                startYPos = car.Passenger.GetCurrentYPos();
                endYPos = car.Passenger.DestinationYPos;
            }

            // -- x axis move logic
            if (startXPos != endXPos)
            {
                if (startXPos < endXPos)
                {
                    if (car.IsRaceCar)
                    {
                        car.MoveRight(startXPos, endXPos);
                        return;
                    }

                    car.MoveRight();
                    return;

                }
                else if (startXPos > endXPos)
                {
                    if (car.IsRaceCar)
                    {
                        car.MoveLeft(startXPos, endXPos);
                        return;
                    }

                    car.MoveLeft();
                    return;
                }
            }

            // -- y axis move logic
            if (startYPos != endYPos)
            {

                if (startYPos > endYPos)
                {
                    if (car.IsRaceCar)
                    {
                        car.MoveDown(startYPos, endYPos);
                        return;
                    }

                    car.MoveDown();
                    return;
                }
                else if (startYPos < endYPos)
                {
                    if (car.IsRaceCar)
                    {
                        car.MoveUp(startYPos, endYPos);
                        return;
                    }

                    car.MoveUp();
                    return;
                }
            }

            // -- set fields when passenger is picked up
            if (transportType == TransportType.PickUp)
            {
                car.PickupPassenger(passenger);
                passenger.GetInCar(car);
            }
        }

        enum TransportType
        {
            PickUp = 0,
            DropOff = 1
        }
    }
}
